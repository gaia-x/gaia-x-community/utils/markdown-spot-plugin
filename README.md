# Gaia-X Trust Framework Single Point of Trust Markdown plugin

Markdown plugin to generate attribute table in the Gaia-X Trust Framework doc, out of the YAML SPoT files.

## Install

```shell
pip install gxTrustFrameworkSPoT --extra-index-url https://gitlab.com/api/v4/projects/38506401/packages/pypi/simple
```

## Usage

```markdown
!includeSPoT https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/raw/develop/single-point-of-truth/yaml/gax-participant/legal-person.yaml
```

### Options

In case the object is not the root element, it's possible to specify a JSONPath

```markdown
!includeSPoT https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/raw/develop/single-point-of-truth/yaml/gax-participant/legal-person.yaml|path=$.LegalPerson
```

## Plugin options

`generateSHACL`: Generate SHACL file from SPoT file (**TODO**). Default false.
