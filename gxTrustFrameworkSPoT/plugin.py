#!/usr/bin/env python
import os
import re
import markdown
import xml.etree.ElementTree as etree
import logging
from urllib.parse import parse_qsl
import requests
import yaml
import jsonpath_ng as jp


logger = logging.getLogger('MARKDOWN')

def createETList(elements, **kwargs):
    result = etree.Element("ul", **kwargs)
    for elt in elements:
        etree.SubElement(result, "li").text = str(elt)
    return result


def createTableHeader(elements, **kwargs):
    result = etree.Element("tr", **kwargs)
    for elt in elements:
        etree.SubElement(result, "th").text = str(elt)
    return result


# For details see https://pythonhosted.org/Markdown/extensions/api.html#blockparser
class GxTFSPoTProcessor(markdown.blockprocessors.BlockProcessor):
    RE = re.compile(r'!includeSPoT +(?P<source>.*)', re.VERBOSE)

    def test(self, parent, block):
        return self.RE.search(block)

    def run(self, parent, blocks):
        assert self.config['generateSHACL'] == False, "SHACL generation is not yet implemented"
        print(self.config)
        block = blocks.pop(0)
        text = block
        # Parse configuration params
        m = self.RE.search(block)
        source = m.group('source').strip()

        options = {}
        if '|' in source:
            source, options = source.split('|', maxsplit=1)
            options = dict(parse_qsl(options, max_num_fields=1))
        if 'path' not in options:
            options['path'] = '$.*'
        print(source, options)
        spot = requests.get(source).text
        doc = yaml.load(spot, Loader=yaml.SafeLoader)
        path = jp.parse(options['path'])
        for match in path.find(doc):
            print("Match", match)
            print("Match", match.value)
            table = etree.SubElement(parent, "table") #, src=imageurl, alt=alt, attrib={'class':classes})
            gxType =  match.full_path.fields[-1]
            etree.SubElement(table, "caption").text = f"{match.value['prefix']}:{gxType}"
            table.append(createTableHeader(["Name", "Cardinality", "Description"]))
            for attribute in match.value['attributes']:
                row = etree.SubElement(table, "tr")
                print(attribute)
                etree.SubElement(row, "td").text = attribute['title']
                etree.SubElement(row, "td").text = f"{attribute['cardinality'].get('minCount', '0')}..{attribute['cardinality'].get('maxCount', '*')}"
                description = etree.SubElement(row, "td")
                description.text = attribute['description']
                description.append(createETList(attribute['exampleValues'], style="exampleValues"))
                print(etree.tostring(row))
