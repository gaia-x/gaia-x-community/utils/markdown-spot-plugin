import markdown
from .plugin import GxTFSPoTProcessor


class GxTFSPoTMarkdownExtension(markdown.Extension):
    def __init__(self, *args, **kwargs):
        print(kwargs)
        self.config = {
            'generateSHACL': [False, "Generate SHACL file from SPoT. Default false"]
        }
        super(GxTFSPoTMarkdownExtension, self).__init__(*args, **kwargs)

    def extendMarkdown(self, md):
        blockprocessor = GxTFSPoTProcessor(md.parser)
        blockprocessor.config = self.getConfigs()
        md.parser.blockprocessors.register(blockprocessor, 'GxSPoT', 100)

def makeExtension(**kwargs):
    return GxTFSPoTMarkdownExtension(**kwargs)
